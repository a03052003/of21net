import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () => new Vuex.Store({
	state: {
		modal: 0,
		text: ""
	},
	mutations: {
		modal(state) {
			state.modal++;
		},
		modal_text(state, text) {
			state.text = text
		}
	},
	getters: {
		modal: state => {
			return state.modal;
		},
		modal_text: state => {
			return state.text;
		}
	},
})

export default store