module.exports = {
	plugins: [
		{ src: '~/plugins/vue-js-modal' },
		{ src: '~/plugins/vue-mask' },
		{ src: '~/plugins/vue-lazyload', ssr: false },
		{ src: '~/plugins/vue-gallery-slideshow', ssr: false }
	],
	head: {
		title: 'Офис 21',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: 'Of21' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
			{ rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto' },
			// { rel: 'stylesheet', href: 'https://itcron.of21.net/assets/callback.css' }
		],
		script: [
			{ src: 'https://code.jquery.com/jquery-3.3.1.min.js', defer: true },
			// { src: 'https://itcron.of21.net/api/js?token=5cc3829b9dab8fa7ccc426bb70079da9', defer: true }
		]
	},
	modules: [
		[
			'@nuxtjs/yandex-metrika',
			{
				id: 21253963,
				webvisor: true,
				clickmap: true,
				trackLinks: true,
				accurateTrackBounce: true,
				trackHash: true
			}
		],
		['@nuxtjs/axios']
	],
	css: [
		'../node_modules/bootstrap/dist/css/bootstrap-grid.css',
		'~/assets/less/fonts.css',
		'~/assets/less/style.less',
		'~/assets/less/media.less'
	],

	loading: { color: '#3B8070' },
	build: {
		extend(config, { isDev, isClient }) {
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/
				});
				// config.module.rules.push({
				// 	test: /\.less$/,
				// 	use: [
				// 		'vue-style-loader',
				// 		'css-loader',
				// 		'less-loader'
				// 	]
				// });
			}
		},
	},
	generate: {
		minify: {
			removeRedundantAttributes: false
		}
	},
	router: {
		// mode: 'history',
		// base: '/',
		// routes: [
		// 	{
		// 		path: '/page',
		// 		name: 'page',
		// 		component: '~/components/Slider.vue'
		// 	}
		// ]
	},
	axios: {
		// proxyHeaders: false
	}
}
