import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _17628202 = () => interopDefault(import('..\\pages\\1c.vue' /* webpackChunkName: "pages_1c" */))
const _2952f470 = () => interopDefault(import('..\\pages\\about.vue' /* webpackChunkName: "pages_about" */))
const _17259aab = () => interopDefault(import('..\\pages\\clients.vue' /* webpackChunkName: "pages_clients" */))
const _3d54ccc0 = () => interopDefault(import('..\\pages\\contacts.vue' /* webpackChunkName: "pages_contacts" */))
const _017cb1c6 = () => interopDefault(import('..\\pages\\control-business.vue' /* webpackChunkName: "pages_control-business" */))
const _07707901 = () => interopDefault(import('..\\pages\\crm.vue' /* webpackChunkName: "pages_crm" */))
const _29c2d56a = () => interopDefault(import('..\\pages\\deals.vue' /* webpackChunkName: "pages_deals" */))
const _59989e4f = () => interopDefault(import('..\\pages\\debit.vue' /* webpackChunkName: "pages_debit" */))
const _10e10602 = () => interopDefault(import('..\\pages\\email.vue' /* webpackChunkName: "pages_email" */))
const _47f4b7e0 = () => interopDefault(import('..\\pages\\extras.vue' /* webpackChunkName: "pages_extras" */))
const _66c3d924 = () => interopDefault(import('..\\pages\\increase-sales.vue' /* webpackChunkName: "pages_increase-sales" */))
const _37756743 = () => interopDefault(import('..\\pages\\loss-money.vue' /* webpackChunkName: "pages_loss-money" */))
const _0d625b5f = () => interopDefault(import('..\\pages\\policy.vue' /* webpackChunkName: "pages_policy" */))
const _21853a61 = () => interopDefault(import('..\\pages\\report.vue' /* webpackChunkName: "pages_report" */))
const _12cdb2e2 = () => interopDefault(import('..\\pages\\reports.vue' /* webpackChunkName: "pages_reports" */))
const _2751e9fb = () => interopDefault(import('..\\pages\\telephony.vue' /* webpackChunkName: "pages_telephony" */))
const _490acf35 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/1c",
    component: _17628202,
    name: "1c"
  }, {
    path: "/about",
    component: _2952f470,
    name: "about"
  }, {
    path: "/clients",
    component: _17259aab,
    name: "clients"
  }, {
    path: "/contacts",
    component: _3d54ccc0,
    name: "contacts"
  }, {
    path: "/control-business",
    component: _017cb1c6,
    name: "control-business"
  }, {
    path: "/crm",
    component: _07707901,
    name: "crm"
  }, {
    path: "/deals",
    component: _29c2d56a,
    name: "deals"
  }, {
    path: "/debit",
    component: _59989e4f,
    name: "debit"
  }, {
    path: "/email",
    component: _10e10602,
    name: "email"
  }, {
    path: "/extras",
    component: _47f4b7e0,
    name: "extras"
  }, {
    path: "/increase-sales",
    component: _66c3d924,
    name: "increase-sales"
  }, {
    path: "/loss-money",
    component: _37756743,
    name: "loss-money"
  }, {
    path: "/policy",
    component: _0d625b5f,
    name: "policy"
  }, {
    path: "/report",
    component: _21853a61,
    name: "report"
  }, {
    path: "/reports",
    component: _12cdb2e2,
    name: "reports"
  }, {
    path: "/telephony",
    component: _2751e9fb,
    name: "telephony"
  }, {
    path: "/",
    component: _490acf35,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
